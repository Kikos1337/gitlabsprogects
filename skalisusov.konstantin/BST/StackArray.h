#ifndef STACK_STACKARRAY_H
#define STACK_STACKARRAY_H
#include "stack.hpp"
#include "WrongStackSize.h"
#include "StackOverflow.h"
#include "StackUnderflow.h"
template <typename T>
class StackArray: public Stack<T>
        {
public:
    StackArray(std::size_t size);
    StackArray(const StackArray<T>& src);
    StackArray(StackArray<T>&& src);
    StackArray& operator=(const StackArray<T>&src);
    StackArray& operator=(StackArray<T>&& src);
    virtual ~StackArray();
    void push(const T& e);
     T&  pop();
     T top();
    bool isEmpty();
    void print();
private:
    T* array_;
    std::size_t top_;
    std::size_t size_;
    void swap(StackArray<T>&src);
};

template<typename T>
void StackArray<T>::swap(StackArray<T> &src)
{
    std::swap(array_,src.array_);
    std::swap(top_,src.top_);
    std::swap(size_,src.size_);
}
template <typename T>
StackArray<T>::StackArray(std::size_t size):
size_(size),
top_(0)
{
    try
    {
        array_=new T[size+1];
    }
    catch (...)
    {
        throw WrongSizeStack();
    }
}
template <typename T>
StackArray<T>::~StackArray()
{
    delete [] array_;
}
template<typename T>
void StackArray<T>::push(const T& e)
{
    if (top_==size_)
    {
        throw StackOverflow();
    }
    array_[top_++]=e;
}
template <typename T>
 T& StackArray<T>::pop()
{
    if (isEmpty())
    {
        throw StackUnderflow();
    }
    else
    {
        return  array_[top_--];
    }
}
template<typename T>
StackArray<T>::StackArray(const StackArray<T>& src):
size_(src.size_),
top_(src.top_)
{
    try
    {
        array_= new T[src.size_+1];
    }
    catch(...)
    {
        throw WrongSizeStack();
    }
    for (int i=1;i<src.top_; i++)
    {
        array_[i]=src.array_[i];
    }
}

template<typename T>
bool StackArray<T>::isEmpty()
{
    return top_==0;
}
template<typename T>
void StackArray<T>::print()
{
    T *p = array_;
    std::cout << "Stack: " << '\n';
    if (size_ == 0)
    {
        std::cout << "is empty." << '\n';
    }
    else
    {
        for (std::size_t i = 0; i <size_; i++)
        {
            std::cout << "Item[" << i << "] = " << *p <<'\n';
            p++;
        }
        std::cout <<'\n';
    }
}
template <typename T>
T StackArray<T>::top()
{
    if (isEmpty())
    {
        throw StackUnderflow();
    }
    else
    {
        return array_[top_-1];
    }
}
#endif //STACK_STACKARRAY_H
