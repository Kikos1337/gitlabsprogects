#ifndef BINARYTREE_BINARYSEARCHTREE_H
#define BINARYTREE_BINARYSEARCHTREE_H

#include <iostream>
#include "StackArray.h"
#include <queue>

template<typename T>
class BinarySearchTree
{
public:
  BinarySearchTree();
  BinarySearchTree(const BinarySearchTree<T> & scr) = delete;
  BinarySearchTree(BinarySearchTree<T>&& scr) noexcept;
  BinarySearchTree <T>& operator= (const BinarySearchTree <T>& src) = delete;
  BinarySearchTree <T>& operator= (BinarySearchTree <T>&& src) noexcept;
  virtual ~BinarySearchTree();

  bool iterativeSearch(const T& key) const;
  bool isEmpty() const;
  bool insert(const T& key);
  bool deleteKey(const T& key);
  void print(std::ostream& out) const;
  int getCount () const;
  int getHeight() const;
  void iterariveInorderWalk() const;
  void recursiveInorderWalk() const;
  void walkByLevels() const;
  bool isSimilar(const BinarySearchTree<T> & other) const;
  bool isIdenticalKey(const BinarySearchTree<T> & other) const;


private:

  template<typename Any>
  struct  Node
  {
    T key_;
    Node<T> *left_;
    Node<T> *right_;
    Node<T> *parents_;
    Node(T key, Node *left = nullptr, Node * right = nullptr,Node *parents = nullptr):
            key_(key), left_(left), right_(right), parents_(parents)
    { };
  };
  Node<T> *root_;

  void inorderWalk(Node<T>* node) const
  {
    if(node != nullptr)
    {
      inorderWalk(node->left_);
      std::cout<< " "<<node->key_;
      inorderWalk(node->right_);
    }
  }
  void deleteTree(const Node<T> *);
  Node<T>* findMaxValues(Node<T>* node)
  {
      if(node->right_ == nullptr)
      {
        return  node;
      }
      else
      {
        return  findMaxValues(node->right_);
      }
  }
  Node<T>* findMinValues(Node<T>* node)
  {
      if ( node->left_ == nullptr)
      {
        return node;
      }
      else
      {
        return  findMinValues(node->left_);
      }
  }
  Node<T>* iterativeSearchNode(const T& key) const  //done
  {
    Node<T> *tpm = root_;

    if(!isEmpty())
    {
      while(tpm != nullptr)
      {
        if(tpm->key_ == key)
        {
          return tpm;
        }
        else
        {
          if(tpm->key_ < key)
          {
            tpm = tpm->right_;
          }
          else
          {
            tpm = tpm->left_;
          }
        }
      }
      return  tpm;
    }
    else
    {
      return tpm;
    }
  }
  bool recusisIdenticalKey(const BinarySearchTree<T> &root , const BinarySearchTree<T>::Node<T> *root2) const;
  void printNode(std::ostream &out, Node<T> *root) const;

  int getHeight(const Node<T>* node) const
  {
    if (node == nullptr)
    {
      return  0;
    }
    else
    {
      int MaxHeigtLeft = getHeight(node->left_);
      int MaxheigtRigt = getHeight(node->right_);

      if(MaxHeigtLeft >= MaxheigtRigt)
      {
        return  1 + MaxHeigtLeft;
      }
      else
      {
        return  1 + MaxheigtRigt;
      }
    }
  }

  int getCount(Node<T> *node) const
  {
    if (node == nullptr)
    {
      return 0;
    }
    return (1 + getCount(node->left_) + getCount(node->right_));
  }
  bool ComparisonKeysTrees(const Node<T> *root1 , const Node<T> *root2) const
  {
    if(root1 == nullptr && root2 == nullptr)
    {
      return true;
    }
    if(root1 == nullptr && root2 != nullptr || root1 != nullptr && root2 == nullptr)
    {
      return false;
    }
    if(root1->key_ != root2->key_)
    {
      return false;
    }
    else
    {
      return ComparisonKeysTrees(root1->left_ , root2->left_) && ComparisonKeysTrees(root1->left_,root2->right_);
    }

  }
};

template<typename T>
BinarySearchTree<T>::BinarySearchTree():
root_(nullptr)
{
}

template<typename T>
BinarySearchTree<T>::~BinarySearchTree()
{
  deleteTree(root_);
  root_ = nullptr;
}

template<typename T>
bool BinarySearchTree<T>::isEmpty() const
{
  if(root_ == nullptr)
  {
    return true;
  }
  return false;
}

template<typename T>
bool BinarySearchTree<T>::insert(const T &key)
{
  Node<T> *tmp = new Node<T>(key);
  if (root_ == nullptr)
  {
    root_ = tmp;
    return true;
  }
  Node<T> *current = root_;
  Node<T> *current_tail = root_;
  while (current != nullptr)
  {
    if (current->key_ < key)
    {
      current_tail = current;
      current = current_tail->right_;
    }
    else if (current->key_ > key)
    {
      current_tail = current;
      current = current->left_;
    }
    else
    {
      // repeating element
      return false;
    }
  }
  if (current_tail->key_ < key)
  {
    current_tail->right_ = tmp;
  }
  else
  {
    current_tail->left_ = tmp;
  }
  tmp->parents_ = current_tail;
  return true;
}

template<typename T>
void BinarySearchTree<T>::deleteTree(const Node<T> *tpm)
{
  if(tpm != nullptr)
  {
    deleteTree(tpm->left_);
    deleteTree(tpm->right_);
    delete tpm;
  }
  else
    return;
}

template<typename T>
void BinarySearchTree<T>::print(std::ostream& out) const
{
  printNode(out,root_);
}

template<typename T>
void BinarySearchTree<T>::printNode(std::ostream &out, Node<T> *root) const
{
  out<<'(';
  if(root)
  {
    out<<root->key_ << " ";
    printNode(out,root->left_);
    printNode(out,root->right_);
  }
  out<<')';
}

template<typename T>
int BinarySearchTree<T>::getCount() const
{
  return getCount(root_);
}

template<typename T>
int BinarySearchTree<T>::getHeight() const
{
 return getHeight(root_);
}


template<typename T>
bool BinarySearchTree<T>::iterativeSearch(const T &key) const
{
  Node<T> *tpm = root_;
  if(!isEmpty())
  {
    while(tpm != nullptr)
    {
      if(tpm->key_ == key)
      {
        return tpm;
      }
      else
      {
        if(tpm->key_ < key)
        {
          tpm = tpm->right_;
        }
        else
        {
          tpm = tpm->left_;
        }
      }
    }
    return  false;
  }
  else
  {
    return false;
  }
}

template<typename T>
bool BinarySearchTree<T>::deleteKey(const T &key)
{
  if(isEmpty())
  {
    return false;
  }
  Node<T> *tmp = root_;
  if(!iterativeSearchNode(key))
  {
    std::cout<<"key not Tree\n";
    return false;
  }
  else
  {
    while(tmp->key_ != key)
    {
      if(tmp->key_>key)
      {
        tmp = tmp->left_;
      }
      else
      {
        tmp = tmp->right_;
      }
    }
    if(tmp->right_ != nullptr && tmp->left_ != nullptr)
    {
      if(tmp->parents_ != nullptr)
      {
        Node<T> *p = tmp->left_;
        Node<T> *elemdel = tmp;
        tmp = findMinValues(tmp->right_);
        tmp->parents_ = elemdel->parents_;
        tmp->parents_->right_ = tmp;
        elemdel->parents_ = tmp;
        tmp->left_ = p;
        delete (elemdel);
        return true;
      }
      else
      {
        Node<T> *p = root_->left_;
        root_ = findMinValues(root_->right_);
        root_->left_ = p;
        root_->parents_ = nullptr;
        delete (tmp);
        return true;
      }
    }
    else if(tmp->right_ == nullptr && tmp->left_ == nullptr)
    {
      Node<T>*par = tmp->parents_;
      par->left_ = nullptr;
       delete(tmp);
      return true;
    }
    else if(tmp->right_ != nullptr && tmp->left_ == nullptr)
    {
      Node<T> *p = tmp;
      tmp = findMinValues(tmp->right_);
      tmp->parents_ = p->parents_;
      tmp->parents_->right_ = tmp;
      p->parents_ = tmp;
      delete (p);
      return true;
    }
    else if(tmp->right_ == nullptr && tmp->left_ != nullptr)
    {
      Node<T> *p = tmp;
      tmp = findMaxValues(tmp->left_);
      tmp->parents_ = p->parents_;
      tmp->parents_->left_ = tmp;
      p->parents_ = tmp;
      delete (p);
      return true;
    }
  }
}

template<typename T>
void BinarySearchTree<T>::recursiveInorderWalk()const
{
  inorderWalk(root_);
}
template<typename T>
void BinarySearchTree<T>::iterariveInorderWalk() const
{
  if(isEmpty())
  {
    return;
  }
  StackArray<Node<T>*> stack(getCount(root_));
  Node<T> *tmp = root_;
  while (tmp != nullptr || !stack.isEmpty())
  {
    while (tmp != nullptr)
    {
      stack.push(tmp);
      tmp = tmp->left_;
    }
    tmp = stack.top();
    stack.pop();
    std::cout<< " " << tmp->key_;
    tmp = tmp->right_;
  }
}

template<typename T>
void BinarySearchTree<T>::walkByLevels() const
{
  if(root_ == nullptr)
  {
    return;
  }
  std::queue<Node<T>*> queue;
  queue.push(root_);
  while(!queue.empty())
  {
    Node<T>* tmp = queue.front();
    queue.pop();
    std::cout<<tmp->key_ << " ";
    if(tmp->left_ != nullptr)
    {
      queue.push(tmp->left_);
    }
    if(tmp->right_ != nullptr)
    {
      queue.push(tmp->right_);
    }
  }
}
template<typename T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T> &other) const
{
  if(root_ == nullptr && other.root_ == nullptr)
  {
    return true;
  }
  if(getCount() != other.getCount())
  {
    return false;
  }
  if(getHeight() != other.getHeight())
  {
    return false;
  }
  if(ComparisonKeysTrees(root_,other.root_))
  {
    return  true;
  }
  else
  {
    return false;
  }
}

template<typename T>
bool BinarySearchTree<T>::recusisIdenticalKey(const BinarySearchTree<T> &root, const BinarySearchTree<T>::Node<T> *root2) const
{
  if(root2 == nullptr)
  {
    return false;
  }
  else
  {
    recusisIdenticalKey(root , root2->right_);
    if(root.iterativeSearchNode(root2->key_))
    {
     return true;
    }
    else
    {
      return recusisIdenticalKey(root, root2->left_);
    }
  }
}
template<typename T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T> &other) const
{
  if(recusisIdenticalKey(other,root_))
  {
    return true;
  }
  else
  {
    return false;
  }
}
#endif //BINARYTREE_BINARYSEARCHTREE_H