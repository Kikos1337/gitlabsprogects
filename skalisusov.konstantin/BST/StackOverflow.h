#ifndef STACK_STACKOVERFLOW_H
#define STACK_STACKOVERFLOW_H
#include <exception>
#include "stack.hpp"
class StackOverflow: public std::exception
{
private:
    const std::string reason_;
public:
    StackOverflow(): reason_("Stack Over Flow") {}
    const std::string& what()   {return reason_;}
};
#endif //STACK_STACKOVERFLOW_H
