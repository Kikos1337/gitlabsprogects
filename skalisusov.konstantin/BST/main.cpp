#include <iostream>

int main()
{
  BinarySearchTree<int> t;
  BinarySearchTree<int> t2;
  t.insert(10);
  t.insert(11);
  t.insert(12);
  t.insert(15);
  t.insert(13);
  t.insert(7);
  t.insert(4);
  t.insert(2);
  t.insert(3);
  t.insert(1);

  t.print(std::cout);
  std::cout<<'\n';
  t.iterativeSearch(3);
  t.deleteKey(10);

  t.print(std::cout);
  std::cout<<'\n';
  std::cout<<t.getCount();
  std::cout<<'\n';
  std::cout<<t.getHeight();
  std::cout<<'\n';
  t.recursiveInorderWalk();
  std::cout<<'\n';
  t.iterariveInorderWalk();
  std::cout<<'\n';
  t.walkByLevels();
  std::cout<<'\n';
  t2.insert(9);
  t2.insert(10);
  t2.insert(11);
  t2.insert(14);
  t2.insert(12);
  t2.insert(5);
  t2.insert(4);
  t2.insert(2);
  t2.insert(3);
  t2.insert(1);
  std::cout<<(t.isSimilar(t2) ? "TRUE" : "FALSE") <<'\n';
  std::cout<<(t.isIdenticalKey(t2) ? "TRUE" : "FALSE");
  return  0;
}