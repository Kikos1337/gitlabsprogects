#ifndef STACK_STACK_HPP
#define STACK_STACK_HPP
template <typename  T>
class Stack
{
public:
    virtual ~Stack() {}
    virtual void push(const T& e)=0;
    virtual T& pop() = 0;
    virtual bool isEmpty()=0;
};
#endif //STACK_STACK_HPP
