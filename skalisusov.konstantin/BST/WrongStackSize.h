#ifndef STACK_WRONGSTACKSIZE_H
#define STACK_WRONGSTACKSIZE_H
#include <exception>
#include "stack.hpp"
class WrongSizeStack: std::exception
{
private:
    const std::string reason_;
public:
    WrongSizeStack() : reason_("Wrong Size Stack") {}
    const std::string& what()  { return  reason_; }
};
#endif //STACK_WRONGSTACKSIZE_H
