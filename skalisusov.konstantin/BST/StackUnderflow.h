#ifndef STACK_STACKUNDERFLOW_H
#define STACK_STACKUNDERFLOW_H
#include <exception>
class StackUnderflow: public std::exception
{
private:
    const std::string reason_;
public:
    StackUnderflow() : reason_("Stack Underflow") {}
    const std::string& what() { return reason_; }
};
#endif //STACK_STACKUNDERFLOW_H
